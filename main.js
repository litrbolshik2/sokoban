
var i;
let arena = new Arena();
let view = new View(arena);

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

async function Strategy()
{
    /* 1 */
    if(0 != view.RobotMove(0, "move", 6))  return;
    await sleep(500);
    if(0 != view.RobotMove(1, "move", 0)) return;
    await sleep(500);
    /* 2 */
    if(0 != view.RobotMove(0, "left", 0)) return;
    await sleep(500);
    if(0 != view.RobotMove(1, "move", 0)) return;
    await sleep(500);
    /* 3 */
    if(0 != view.RobotMove(0, "left", 0)) return;
    await sleep(500);
    if(0 != view.RobotMove(1, "move", 0)) return;
    await sleep(500);
    /* 4 */
    if(0 != view.RobotMove(0, "move", 0)) return;
    await sleep(500);
    if(0 != view.RobotMove(1, "left", 0)) return;
    await sleep(500);
    /* 5 */
    if(0 != view.RobotMove(0, "move", 0)) return;
    await sleep(500);
    if(0 != view.RobotMove(1, "move", 0)) return;
    await sleep(500);
    /* 6 */
    if(0 != view.RobotMove(0, "move", 0)) return;
    await sleep(500);
    if(0 != view.RobotMove(1, "left", 0)) return;
    await sleep(500);
    /* 7 */
    if(0 != view.RobotMove(0, "move", 0)) return;
    await sleep(500);
    if(0 != view.RobotMove(1, "left", 0)) return;
    await sleep(500);
    /* 8 */
    if(0 != view.RobotMove(0, "left", 0)) return;
    await sleep(500);
    if(0 != view.RobotMove(1, "move", 0)) return;
    await sleep(500);
    /* 9 */
    if(0 != view.RobotMove(0, "left", 0)) return;
    await sleep(500);
    if(0 != view.RobotMove(1, "left", 0)) return;
    await sleep(500);
    /* 10 */
    if(0 != view.RobotMove(0, "move", 0)) return;
    await sleep(500);
    if(0 != view.RobotMove(1, "move", 3))  return;
    await sleep(500);
    /* 11 */
    if(0 != view.RobotMove(0, "move", 0)) return;
    await sleep(500);
    if(0 != view.RobotMove(1, "left", 0))  return;
    await sleep(500);
    /* 12 */
    if(0 != view.RobotMove(0, "right", 0)) return;
    await sleep(500);
    if(0 != view.RobotMove(1, "left", 0))  return;
    await sleep(500);
    /* 13 */
    if(0 != view.RobotMove(0, "move", 0)) return;
    await sleep(500);
    if(0 != view.RobotMove(1, "move", 0))  return;
    await sleep(500);
    /* 14 */
    if(0 != view.RobotMove(0, "left", 0)) return;
    await sleep(500);
    if(0 != view.RobotMove(1, "move", 0))  return;
    await sleep(500);
    /* 15 */
    if(0 != view.RobotMove(0, "left", 0)) return;
    await sleep(500);
    if(0 != view.RobotMove(1, "move", 0))  return;
    await sleep(500);
    /* 16 */
    if(0 != view.RobotMove(0, "move", 0)) return;
    await sleep(500);
    if(0 != view.RobotMove(1, "move", 0))  return;
    await sleep(500);
}

view.ArenaShow(arena);

