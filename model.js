
class Cell {
    constructor(x, y) {
	this.x = x;
	this.y = y;
    }
}

class Robot extends Cell {
    constructor(x, y, angle_x, angle_y, txt_id, color) {
	super(x, y);
	this.angle_x = angle_x;
	this.angle_y = angle_y;
	this.moves = 0;
	this.patterns = 1;
	this.txt_id = txt_id;
	this.color = color;
	this.step = 0;
    }

    GetAngle() {
	if(this.angle_x ==  1 && this.angle_y ==  0) return 0;
	if(this.angle_x ==  0 && this.angle_y ==  1) return 90;
	if(this.angle_x == -1 && this.angle_y ==  0) return 180;
	if(this.angle_x ==  0 && this.angle_y == -1) return -90;
	return NaN;
    }

    GetAngleRad() {
	return this.GetAngle() * Math.PI / 180;
    }

    isPatternsAvail() {
	return this.patterns > 0;
    }

    Msg(text) {
	var msg = "step" + this.step + ":" + text;
	document.getElementById(this.txt_id).innerHTML = msg;
    }

    NextStep() {
	this.step++;
    }

    UsePattern() {
	if(this.patterns > 0) this.patterns--;
    }

}

class Arena {
    constructor () {
	this.cells_x = 3;
	this.cells_y = 8;
	var x, y;
	this.cells = [];
	for(x = 0; x < this.cells_x; x++) {
	    for(y = 0; y < this.cells_y; y++) {
		var cell = new Cell(x, y);
		this.cells.push(cell);
	    }
	}
	this.targets = [];
	this.targets.push(new Cell(1, 3));
	this.targets.push(new Cell(1, 4));
	this.bochkas = [];
	this.bochkas.push(new Cell(1, 2));
	this.bochkas.push(new Cell(1, 5));
	this.boxes = [];
	this.boxes.push(new Cell(0, 7));
	this.boxes.push(new Cell(2, 7));
	this.robots = [];
	this.robots.push(new Robot(0, 0, 0, 1, "log_blue", "blue"));
	this.robots.push(new Robot(2, 0, 0, 1, "log_red", "red"));
    }

    GetCells() {
	return this.cells;
    }
    
    GetTargets() {
	return this.targets;
    }

    GetBochkas() {
	return this.bochkas;
    }

    GetBoxes() {
	return this.boxes;
    }

    GetRobots() {
	return this.robots;
    }

    isInArea(x, y) {
	if(x < 0 || y < 0) return false;
	if(x >= this.cells_x) return false;
	if(y >= this.cells_y) return false;
	return true;
    }

    isBusyForObject(new_x, new_y, cells) {
	for(i = 0; i < cells.length; i++) {
	    var c = cells[i];
	    if(new_x == c.x && new_y == c.y) return true;
	}
	return false;
    }

    isBusy(new_x, new_y) {
	if(this.isBusyForObject(new_x, new_y, this.bochkas)) return true;
	if(this.isBusyForObject(new_x, new_y, this.boxes)) return true;
	/* TODO: check robots collision */
	return false;
    }

    RobotDoMove(robot) {
	var new_x = robot.x + robot.angle_x;
	var new_y = robot.y + robot.angle_y;
	if(!this.isInArea(new_x, new_y)) {
	    robot.Msg("not in area");
	    return 1;
	}
	if(this.isBusy(new_x, new_y)) {
	    robot.Msg("cell is busy");
	    return 1;
	}
	robot.x = new_x;
	robot.y = new_y;
	return 0;
    }

    RobotRotate(robot, dir) {
	if(dir == "left") {
	    if(robot.angle_x == 1 && robot.angle_y == 0 ) {
		robot.angle_x = 0;
		robot.angle_y = 1;
		return 0;
	    }
	    if(robot.angle_x == 0 && robot.angle_y == 1 ) {
		robot.angle_x = -1;
		robot.angle_y = 0;
		return 0;
	    }
	    if(robot.angle_x == -1 && robot.angle_y == 0 ) {
		robot.angle_x = 0;
		robot.angle_y = -1;
		return 0;
	    }
	    if(robot.angle_x == 0 && robot.angle_y == -1 ) {
		robot.angle_x = 1;
		robot.angle_y = 0;
		return 0;
	    }
	} else {
	    if(robot.angle_x == 1 && robot.angle_y == 0 ) {
		robot.angle_x = 0;
		robot.angle_y = -1;
		return 0;
	    }
	    if(robot.angle_x == 0 && robot.angle_y == 1 ) {
		robot.angle_x = 1;
		robot.angle_y = 0;
		return 0;
	    }
	    if(robot.angle_x == -1 && robot.angle_y == 0 ) {
		robot.angle_x = 0;
		robot.angle_y = 1;
		return 0;
	    }
	    if(robot.angle_x == 0 && robot.angle_y == -1 ) {
		robot.angle_x = -1;
		robot.angle_y = 0;
		return 0;
	    }
	}
    }

	EvaluateCargoObject(robot, cargo_obj) {
		var back_cell_x = robot.x - robot.angle_x;
		var back_cell_y = robot.y - robot.angle_y;
		if(cargo_obj.x == back_cell_x && cargo_obj.y == back_cell_y) {
			console.log("hit");
			cargo_obj.x += robot.angle_x;
			cargo_obj.y += robot.angle_y;
		}
	}

	EvaluateCargo(robot, cargo) {
		for(i = 0; i < cargo.length; i++) {
			this.EvaluateCargoObject(robot, cargo[i]);
		}
	}

    RobotMoveCmd(robot, cmd) { 
	robot.Msg(cmd);
	if(cmd == "move") {
	    this.EvaluateCargo(robot, this.GetBoxes());
	    this.EvaluateCargo(robot, this.GetBochkas());
	    if(0 != this.RobotDoMove(robot)) return 1;
	}
	if(cmd == "left" || cmd == "right") {
	    if(0 != this.RobotRotate(robot, cmd)) return 1;
	}
	if(cmd == "idle") {
	    return 0;
	}
	return 0;
    }

    RobotMove(i, cmd, pattern) {
	var rbot = this.robots[i];
	rbot.NextStep();
	if(0 != pattern) {
	    if(rbot.isPatternsAvail()) {
		rbot.UsePattern();
		for(i = 0; i < pattern; i++) {
		    var ret = this.RobotMoveCmd(rbot, cmd);
		    if(0 != ret) return ret;
		}
	    } else {
		rbot.Msg("ERROR: no more patterns to use");
		return 1;
	    }
	} else {
	    return this.RobotMoveCmd(rbot, cmd);
	}
	return 0;
    }
}
