
class View {
    
    constructor(arena) {
	var canvas = document.getElementById("cnv");
	this.ctx = canvas.getContext("2d");
	var c_width = canvas.width;
	var c_height = canvas.height;;
	this.cells_x = arena.cells_x;
	this.cells_y = arena.cells_y;
	this.arena = arena;
	this.cell_size = this.CellCalcSize(c_width, c_height, arena.cells_x, arena.cells_y);
	this.cell_center = this.cell_size / 2;
	this.offset_x = (c_width - (this.cell_size * arena.cells_x)) / 2;
	this.offset_y = (c_height - (this.cell_size * arena.cells_y)) / 2;
    }

    CellCalcSize(c_width, c_height, cells_x, cells_y) {
	var size_x = c_width / cells_x;
	var size_y = c_height / cells_y;
	var size = (size_x < size_y)? size_x : size_y;
	return size * 0.9;
    }

    CellX(x) {
	return this.cell_size * x + this.offset_x + this.cell_center;
    }

    CellY(y) {
	return this.cell_size * (this.cells_y - y - 1) + this.offset_y + this.cell_center;
    }

    CellShow(cell) {
	this.ctx.beginPath();
	this.ctx.strokeStyle = 'Black';
	this.ctx.rect(this.CellX(cell.x) - this.cell_center, 
		      this.CellY(cell.y) - this.cell_center,
		      this.cell_size, this.cell_size);
	this.ctx.stroke(); 
    }

    TargetShow(target) {
	var delta = this.cell_size * 0.3;
	var cell_x0 = this.CellX(target.x);
	var cell_y0 = this.CellY(target.y);
	this.ctx.strokeStyle = 'green';
	this.ctx.shadowBlur = 5;
	this.ctx.lineWidth = 2;
	this.ctx.beginPath();
	this.ctx.moveTo(cell_x0 - delta, cell_y0 - delta);
	this.ctx.lineTo(cell_x0 + delta, cell_y0 + delta);
	this.ctx.stroke();
	this.ctx.moveTo(cell_x0 + delta,  cell_y0 - delta);
	this.ctx.lineTo(cell_x0 - delta,  cell_y0 + delta);
	this.ctx.stroke();
    }

    BochkaShow(bochka) {
	var cell_x0 = this.CellX(bochka.x);
	var cell_y0 = this.CellY(bochka.y);
	var r = (this.cell_size / 2) * 0.6;
	this.ctx.strokeStyle = 'GoldenRod';
	this.ctx.beginPath();
	this.ctx.shadowBlur = 0;
	this.ctx.lineWidth = 2;
	this.ctx.arc(cell_x0, cell_y0, r, 0, 2 * Math.PI);
	this.ctx.stroke();
	this.ctx.fillStyle = "yellow";
	this.ctx.fill();
    }

    BoxShow(box) {
	var center = this.cell_size / 2;
	var r = (this.cell_size / 2) * 0.6;
	this.ctx.strokeStyle = 'GoldenRod';
	this.ctx.beginPath();
	var d = 2 * r;
	this.ctx.rect(this.CellX(box.x) - r,  this.CellY(box.y) - r, d, d);
	this.ctx.stroke();
	this.ctx.fillStyle = "yellow";
	this.ctx.fill();

    }

    RobotShow(robot) {
	var center = this.cell_size / 2;
	var r = (this.cell_size / 2) * 0.6;
	var cell_x0 = this.CellX(robot.x);
	var cell_y0 = this.CellY(robot.y);
	var robot_angle = robot.GetAngleRad();

	this.ctx.translate(cell_x0, cell_y0);
	this.ctx.rotate(-robot_angle);
	this.ctx.translate(-cell_x0, -cell_y0);

	this.ctx.strokeStyle = robot.color;
	this.ctx.lineWidth = 2;
	this.ctx.beginPath();
	this.ctx.moveTo(cell_x0 - r, cell_y0 + r);
	this.ctx.lineTo(cell_x0 - r, cell_y0 - r);
	this.ctx.lineTo(cell_x0 + r, cell_y0);
	this.ctx.closePath();
	this.ctx.stroke();

	this.ctx.translate(cell_x0, cell_y0);
	this.ctx.rotate(robot_angle);
	this.ctx.translate(-cell_x0, -cell_y0);
	this.ctx.fillStyle = robot.color;
	this.ctx.fill();
    }

    ArenaShow(arena) {
	this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
	var cells = arena.GetCells();
	for(i = 0; i < cells.length; i++) {
	    view.CellShow(cells[i]);
	}
	var targets = arena.GetTargets();
	for(i = 0; i < targets.length; i++) {
	    view.TargetShow(targets[i]);
	}
	var bochkas = arena.GetBochkas();
	for(i = 0; i < bochkas.length; i++) {
	    view.BochkaShow(bochkas[i]);
	}
	var boxes = arena.GetBoxes();
	for(i = 0; i < boxes.length; i++) {
	    view.BoxShow(boxes[i]);
	}
	var robots = arena.GetRobots();
	for(i = 0; i < robots.length; i++) {
	    view.RobotShow(robots[i]);
	}
    }
 
    RobotMove(i, cmd, pattern) {
	var ret = this.arena.RobotMove(i, cmd, pattern);
	if(0 != ret) return ret;
	this.ArenaShow(this.arena);
	return 0;
    }

}

